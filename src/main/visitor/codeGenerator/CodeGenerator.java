    package main.visitor.codeGenerator;

import main.ast.nodes.Program;
import main.ast.nodes.declaration.classDec.ClassDeclaration;
import main.ast.nodes.declaration.classDec.classMembersDec.ConstructorDeclaration;
import main.ast.nodes.declaration.classDec.classMembersDec.FieldDeclaration;
import main.ast.nodes.declaration.classDec.classMembersDec.MethodDeclaration;
import main.ast.nodes.declaration.variableDec.VarDeclaration;
import main.ast.nodes.expression.*;
import main.ast.nodes.expression.operators.BinaryOperator;
import main.ast.nodes.expression.operators.UnaryOperator;
import main.ast.nodes.expression.values.ListValue;
import main.ast.nodes.expression.values.NullValue;
import main.ast.nodes.expression.values.primitive.BoolValue;
import main.ast.nodes.expression.values.primitive.IntValue;
import main.ast.nodes.expression.values.primitive.StringValue;
import main.ast.nodes.statement.*;
import main.ast.nodes.statement.loop.BreakStmt;
import main.ast.nodes.statement.loop.ContinueStmt;
import main.ast.nodes.statement.loop.ForStmt;
import main.ast.nodes.statement.loop.ForeachStmt;
import main.ast.types.NullType;
import main.ast.types.Type;
import main.ast.types.functionPointer.FptrType;
import main.ast.types.list.ListNameType;
import main.ast.types.list.ListType;
import main.ast.types.single.BoolType;
import main.ast.types.single.ClassType;
import main.ast.types.single.IntType;
import main.ast.types.single.StringType;
import main.symbolTable.SymbolTable;
import main.symbolTable.exceptions.ItemNotFoundException;
import main.symbolTable.items.ClassSymbolTableItem;
import main.symbolTable.items.FieldSymbolTableItem;
import main.symbolTable.utils.graph.Graph;
import main.visitor.Visitor;
import main.visitor.typeChecker.ExpressionTypeChecker;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

public class CodeGenerator extends Visitor<String> {
    ExpressionTypeChecker expressionTypeChecker;
    Graph<String> classHierarchy;
    private String outputPath;
    private FileWriter currentFile;
    private ClassDeclaration currentClass;
    private MethodDeclaration currentMethod;
    private final Map<Class, String> objectTypeMapping;
    private int tempSlotCounter = 0;
    private int globalLabelCounter = 0;
    private final Stack<String> breakLabels = new Stack<>();
    private final Stack<String> continueLabels = new Stack<>();

    public CodeGenerator(Graph<String> classHierarchy) {
        this.classHierarchy = classHierarchy;
        this.expressionTypeChecker = new ExpressionTypeChecker(classHierarchy);
        this.prepareOutputFolder();

        objectTypeMapping = Map.of(
            IntType.class, "java/lang/Integer",
            BoolType.class, "java/lang/Boolean",
            StringType.class, "java/lang/String",
            FptrType.class, "Fptr",
            ListType.class, "List"
        );
    }

    public String getTypeMapping(Type type) {
        if (type instanceof ClassType) {
            return ((ClassType) type).getClassName().getName();
        }
        else {
            return objectTypeMapping.get(type.getClass());
        }
    }

    private String toPrimitive(Type type) {
        String result = "";
        if (type instanceof IntType) {
            result = "invokevirtual java/lang/Integer/intValue()I\n";
        }
        else if (type instanceof BoolType) {
            result = "invokevirtual java/lang/Boolean/booleanValue()Z\n";
        }
        return result;
    }

    private String toObj(Type type) {
        String result = "";
        if (type instanceof IntType) {
            result = "invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;\n";
        }
        else if (type instanceof BoolType) {
            result = "invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;\n";
        }
        return result;
    }

    private String emptyArrayList() {
        String commands = "new java/util/ArrayList\n";
        commands += "dup\n";
        commands += "invokespecial java/util/ArrayList/<init>()V\n";
        return commands;
    }

    private String newList(int argumentsSlot) {
        return "new List\n" +
            "dup\n" +
            "aload " + argumentsSlot + "\n" +
            "invokespecial List/<init>(Ljava/util/ArrayList;)V\n";
    }

    private String cast (Type type) {
        return "checkcast " + getTypeMapping(type) + "\n";
    }

    private String newLabel() {
        return String.valueOf(globalLabelCounter++);
    }

    private void initializeVar (Type type) {
        addCommand("; --- init values ---");
        if (type instanceof ClassType || type instanceof FptrType)
            addCommand("aconst_null");
        else if (type instanceof IntType) {
            addCommand("ldc 0");
            addCommand(toObj(type));
        } else if (type instanceof BoolType) {
            addCommand("ldc 0");
            addCommand(toObj(type));
        } else if (type instanceof StringType)
            addCommand("ldc \"\"");
        else if (type instanceof ListType) {
            addCommand(emptyArrayList());
            int arraySlot = slotOf("");
            addCommand("astore " + arraySlot + "\n");
            for (ListNameType element : ((ListType) type).getElementsTypes()) {
                addCommand("aload " + arraySlot);
                initializeVar(element.getType());
                addCommand("invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z");
                addCommand("pop");
            }
            addCommand(newList(arraySlot));
        }
    }

    private void initializeClassFields() {
        for (FieldDeclaration fieldDeclaration : currentClass.getFields()) {
            addCommand("aload 0");
            Type varType = fieldDeclaration.getVarDeclaration().getType();
            String varName = fieldDeclaration.getVarDeclaration().getVarName().getName();
            initializeVar(varType);
            addCommand("putfield " + currentClass.getClassName().getName() + "/" + varName + " " + makeTypeSignature(varType));
        }
    }

    private String compare(String operator, String objectType) {
        StringBuilder commands = new StringBuilder();
        String label = newLabel();
        String conditionTrue = "conditionTrue_" + label;
        String end = "compareEnd_" + label;
        String type = objectType.equals("i") ? "icmp" : "acmp";
        commands.append("if_").append(type).append(operator).append(" ").append(conditionTrue).append("\n");
        commands.append("iconst_0\n");
        commands.append("goto ").append(end).append("\n");
        commands.append(conditionTrue).append(":\n");
        commands.append("iconst_1\n");
        commands.append(end).append(":\n");
        return commands.toString();
    }

    private void prepareOutputFolder() {
        this.outputPath = "output/";
        String jasminPath = "utilities/jarFiles/jasmin.jar";
        String listClassPath = "utilities/codeGenerationUtilityClasses/List.j";
        String fptrClassPath = "utilities/codeGenerationUtilityClasses/Fptr.j";
        try{
            File directory = new File(this.outputPath);
            File[] files = directory.listFiles();
            if(files != null)
                for (File file : files)
                    file.delete();
            directory.mkdir();
        }
        catch(SecurityException e) { }
        copyFile(jasminPath, this.outputPath + "jasmin.jar");
        copyFile(listClassPath, this.outputPath + "List.j");
        copyFile(fptrClassPath, this.outputPath + "Fptr.j");
    }

    private void copyFile(String toBeCopied, String toBePasted) {
        try {
            File readingFile = new File(toBeCopied);
            File writingFile = new File(toBePasted);
            InputStream readingFileStream = new FileInputStream(readingFile);
            OutputStream writingFileStream = new FileOutputStream(writingFile);
            byte[] buffer = new byte[1024];
            int readLength;
            while ((readLength = readingFileStream.read(buffer)) > 0)
                writingFileStream.write(buffer, 0, readLength);
            readingFileStream.close();
            writingFileStream.close();
        } catch (IOException e) { }
    }

    private void createFile(String name) {
        try {
            String path = this.outputPath + name + ".j";
            File file = new File(path);
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(path);
            this.currentFile = fileWriter;
        } catch (IOException e) {}
    }

    private void addCommand(String command) {
        try {
            command = String.join("\n\t\t", command.split("\n"));
            if (command.startsWith("Label_"))
                this.currentFile.write("\t" + command + "\n");
            else if (command.startsWith("."))
                this.currentFile.write(command + "\n");
            else
                this.currentFile.write("\t\t" + command + "\n");
            this.currentFile.flush();
        } catch (IOException e) {}
    }

    private String stackInitializationData() {
        int limit = 128;
        return ".limit stack " + limit + "\n" + ".limit locals " + limit;
    }


    private String makeTypeSignature(Type t) {
        return "L" + getTypeMapping(t) + ";";
    }

    private void addDefaultConstructor() {
        this.currentMethod = new MethodDeclaration(new Identifier("tempDefaultConstructor"), new NullType());
        addCommand(".method public <init>()V");
        addCommand(stackInitializationData());
        addCommand("aload 0");
        String parentClassName = currentClass.getParentClassName() == null
            ? "java/lang/Object" : currentClass.getParentClassName().getName();
        addCommand("invokespecial " + parentClassName + "/<init>()V");
        initializeClassFields();
        addCommand("return");
        addCommand(".end method");
    }

    private void addStaticMainMethod() {
        addCommand(".method public static main([Ljava/lang/String;)V");
        addCommand(stackInitializationData());
        addCommand("new Main");
        addCommand("invokespecial Main/<init>()V");
        addCommand("return");
        addCommand(".end method");
    }

    private int slotOf(String identifier) {
        int index = 1;
        ArrayList<VarDeclaration> locals = new ArrayList<>(currentMethod.getArgs());
        locals.addAll(currentMethod.getLocalVars());
        for (VarDeclaration varDec : locals) {
            if (varDec.getVarName().getName().equals(identifier)) {
                return index;
            }
            index++;
        }
        if (identifier.isEmpty()) {
            index += ++tempSlotCounter;
        }
        return index;
    }

    @Override
    public String visit(Program program) {
        for (ClassDeclaration classDeclaration : program.getClasses()) {
            currentClass = classDeclaration;
            expressionTypeChecker.setCurrentClass(classDeclaration);
            classDeclaration.accept(this);
        }
        return null;
    }

    @Override
    public String visit(ClassDeclaration classDeclaration) {
        createFile(classDeclaration.getClassName().getName());
        addCommand(".class public " + classDeclaration.getClassName().getName());
        String parentClassName = currentClass.getParentClassName() == null
            ? "java/lang/Object" : currentClass.getParentClassName().getName();
        addCommand(".super " + parentClassName);
        for (FieldDeclaration fieldDeclaration : classDeclaration.getFields()) {
            fieldDeclaration.accept(this);
        }
        if (classDeclaration.getConstructor() != null) {
            this.currentMethod = classDeclaration.getConstructor();
            expressionTypeChecker.setCurrentMethod(this.currentMethod);
            this.tempSlotCounter = 0;
            classDeclaration.getConstructor().accept(this);
        }
        else {
            addDefaultConstructor();
        }
        for (MethodDeclaration methodDeclaration : classDeclaration.getMethods()) {
            this.currentMethod = methodDeclaration;
            expressionTypeChecker.setCurrentMethod(this.currentMethod);
            this.tempSlotCounter = 0;
            methodDeclaration.accept(this);
        }
        return null;
    }

    @Override
    public String visit(ConstructorDeclaration constructorDeclaration) {
        if (currentClass.getClassName().getName().equals("Main"))
            addStaticMainMethod();
        if (constructorDeclaration.getArgs().size() > 0)
            addDefaultConstructor();
        currentMethod = constructorDeclaration;
        this.visit((MethodDeclaration) constructorDeclaration);
        return null;
    }

    @Override
    public String visit(MethodDeclaration methodDeclaration) {
        StringBuilder args = new StringBuilder();
        for (VarDeclaration varDeclaration : methodDeclaration.getArgs()) {
            args.append(makeTypeSignature(varDeclaration.getType()));
        }
        String methodName;
        String returnType;
        if (methodDeclaration instanceof ConstructorDeclaration) {
            methodName = "<init>";
            returnType = "V";
        }
        else {
            methodName = methodDeclaration.getMethodName().getName();
            returnType = (methodDeclaration.getReturnType() instanceof NullType) ? "V" : makeTypeSignature(methodDeclaration.getReturnType());
        }
        addCommand(".method public " + methodName + "(" + args.toString() +")" + returnType);
        addCommand(stackInitializationData());
        if (methodDeclaration instanceof ConstructorDeclaration) {
            addCommand("aload 0");
            String parentClassName = currentClass.getParentClassName() == null
                ? "java/lang/Object" : currentClass.getParentClassName().getName();
            addCommand("invokespecial " + parentClassName + "/<init>()V\n");
            initializeClassFields();
        }
        for (VarDeclaration varDeclaration : methodDeclaration.getLocalVars())
            varDeclaration.accept(this);
        for (Statement statement : methodDeclaration.getBody())
            statement.accept(this);
        addCommand("return");
        addCommand(".end method");
        return null;
    }

    @Override
    public String visit(FieldDeclaration fieldDeclaration) {
        addCommand(".field " + fieldDeclaration.getVarDeclaration().getVarName().getName() + " "
            + makeTypeSignature(fieldDeclaration.getVarDeclaration().getType()));
        return null;
    }

    @Override
    public String visit(VarDeclaration varDeclaration) {
        initializeVar(varDeclaration.getType());
        addCommand("astore " + slotOf(varDeclaration.getVarName().getName()));
        return null;
    }

    @Override
    public String visit(AssignmentStmt assignmentStmt) {
        BinaryExpression binaryExpression = new BinaryExpression(assignmentStmt.getlValue(), assignmentStmt.getrValue(),
            BinaryOperator.assign);
        addCommand(binaryExpression.accept(this));
        addCommand("pop\n");
        return null;
    }

    @Override
    public String visit(BlockStmt blockStmt) {
        for (Statement statement : blockStmt.getStatements()) {
            statement.accept(this);
        }
        return null;
    }

    @Override
    public String visit(ConditionalStmt conditionalStmt) {
        String label = newLabel();
        String thenLabel = "thenLabel_" + label;
        String elseLabel = "elseLabel_" + label;
        String afterLabel = "afterLabel_" + label;
        addCommand(conditionalStmt.getCondition().accept(this));
        addCommand("ifeq " + elseLabel);
        addCommand(thenLabel + ":");
        conditionalStmt.getThenBody().accept(this);
        addCommand("goto " + afterLabel);
        addCommand(elseLabel + ":");
        if (conditionalStmt.getElseBody() != null)
            conditionalStmt.getElseBody().accept(this);
        addCommand(afterLabel + ":");
        return null;
    }

    @Override
    public String visit(MethodCallStmt methodCallStmt) {
        expressionTypeChecker.setIsInMethodCallStmt(true);
        addCommand(methodCallStmt.getMethodCall().accept(this));
        addCommand("pop\n");
        expressionTypeChecker.setIsInMethodCallStmt(false);
        return null;
    }

    @Override
    public String visit(PrintStmt print) {
        addCommand("getstatic java/lang/System/out Ljava/io/PrintStream;");
        Expression arg = print.getArg();
        addCommand(arg.accept(this));
        Type argType = arg.accept(expressionTypeChecker);
        String printArgType;
        if (argType instanceof IntType)
            printArgType = "I";
        else if (argType instanceof BoolType)
            printArgType = "Z";
        else
            printArgType = "Ljava/lang/String;";
        addCommand("invokevirtual java/io/PrintStream/print(" + printArgType + ")V");
        return null;
    }

    @Override
    public String visit(ReturnStmt returnStmt) {
        Type type = returnStmt.getReturnedExpr().accept(expressionTypeChecker);
        addCommand(returnStmt.getReturnedExpr().accept(this));
        if(type instanceof NullType) {
            addCommand("return");
            return null;
        }
        else if (type instanceof IntType || type instanceof BoolType){
            addCommand(toObj(type));
        }
        addCommand("areturn");
        return null;
    }

    @Override
    public String visit(BreakStmt breakStmt) {
        addCommand("goto " + breakLabels.peek());
        return null;
    }

    @Override
    public String visit(ContinueStmt continueStmt) {
        addCommand("goto " + continueLabels.peek());
        return null;
    }

    @Override
    public String visit(ForeachStmt foreachStmt) {
        String label = newLabel();
        String conditionLabel = "foreachCondition_" + label;
        String updateLabel = "foreachUpdate_" + label;
        String afterLabel = "foreachAfter_" + label;
        continueLabels.push(updateLabel);
        breakLabels.push(afterLabel);

        int iterSlot = slotOf("");
        addCommand("iconst_0");
        addCommand("istore " + iterSlot);
        addCommand(conditionLabel + ":");
        addCommand("iload " + iterSlot);
        addCommand(foreachStmt.getList().accept(this));
        addCommand("getfield List/elements Ljava/util/ArrayList;");
        addCommand("invokevirtual java/util/ArrayList/size()I");
        addCommand(compare("lt", "i"));
        addCommand("ifeq " + afterLabel);
        addCommand(foreachStmt.getList().accept(this));
        addCommand("iload " + iterSlot);
        addCommand("invokevirtual List/getElement(I)Ljava/lang/Object;\n");
        addCommand(cast(foreachStmt.getVariable().accept(expressionTypeChecker)));
        addCommand("astore " + slotOf(foreachStmt.getVariable().getName()));
        if (foreachStmt.getBody() != null) {
            foreachStmt.getBody().accept(this);
        }
        addCommand(updateLabel + ":");
        addCommand("iload " + iterSlot);
        addCommand("iconst_1");
        addCommand("iadd");
        addCommand("istore " + iterSlot);
        addCommand("goto " + conditionLabel);
        addCommand(afterLabel + ":");

        continueLabels.pop();
        breakLabels.pop();
        return null;
    }

    @Override
    public String visit(ForStmt forStmt) {
        String label = newLabel();
        String conditionLabel = "conditionLabel_" + label;
        String updateLabel = "updateLabel_" + label;
        String afterLabel = "forAfterLabel_" + label;
        continueLabels.push(updateLabel);
        breakLabels.push(afterLabel);
        if (forStmt.getInitialize() != null)
            forStmt.getInitialize().accept(this);
        addCommand(conditionLabel + ":");
        if (forStmt.getCondition() != null)
            addCommand(forStmt.getCondition().accept(this));
        addCommand("ifeq " + afterLabel);
        if (forStmt.getBody() != null)
            forStmt.getBody().accept(this);
        addCommand(updateLabel + ":");
        if (forStmt.getUpdate() != null)
            forStmt.getUpdate().accept(this);
        addCommand("goto " + conditionLabel);
        addCommand(afterLabel + ":");
        continueLabels.pop();
        breakLabels.pop();
        return null;
    }

    public String assign(BinaryExpression binaryExpression) {
        StringBuilder commands = new StringBuilder();
        Type firstType = binaryExpression.getFirstOperand().accept(expressionTypeChecker);
        StringBuilder secondOperandCommands = new StringBuilder();
        secondOperandCommands.append(binaryExpression.getSecondOperand().accept(this));
        if (firstType instanceof ListType) {
            int tempSlot = slotOf("");
            secondOperandCommands.append("astore ").append(tempSlot).append("\n");
            secondOperandCommands.append("new List\n");
            secondOperandCommands.append("dup\n");
            secondOperandCommands.append("aload ").append(tempSlot).append("\n");
            secondOperandCommands.append("invokespecial List/<init>(LList;)V\n");
        }
        secondOperandCommands.append(toObj(binaryExpression.getSecondOperand().accept(expressionTypeChecker)));
        if (binaryExpression.getFirstOperand() instanceof Identifier) {
            int slot = slotOf(((Identifier) binaryExpression.getFirstOperand()).getName());
            commands.append(secondOperandCommands);
            commands.append("astore ").append(slot).append("\n");
        }
        else if (binaryExpression.getFirstOperand() instanceof ListAccessByIndex) {
            commands.append(((ListAccessByIndex) binaryExpression.getFirstOperand()).getInstance().accept(this));
            commands.append(((ListAccessByIndex) binaryExpression.getFirstOperand()).getIndex().accept(this));
            commands.append(secondOperandCommands);
            commands.append("invokevirtual List/setElement(ILjava/lang/Object;)V\n");
        }
        else if (binaryExpression.getFirstOperand() instanceof ObjectOrListMemberAccess) {
            Expression instance = ((ObjectOrListMemberAccess) binaryExpression.getFirstOperand()).getInstance();
            Type memberType = binaryExpression.getFirstOperand().accept(expressionTypeChecker);
            String memberName = ((ObjectOrListMemberAccess) binaryExpression.getFirstOperand()).getMemberName().getName();
            Type instanceType = instance.accept(expressionTypeChecker);
            commands.append(instance.accept(this));
            if (instanceType instanceof ListType) {
                int index = 0;
                for (ListNameType element : ((ListType) instanceType).getElementsTypes()) {
                    if (element.getName().getName().equals(memberName)) {
                        break;
                    }
                    index++;
                }
                commands.append("ldc ").append(index).append("\n");
                commands.append(secondOperandCommands);
                commands.append("invokevirtual List/setElement(ILjava/lang/Object;)V\n");
            }
            else if (instanceType instanceof ClassType) {
                commands.append(secondOperandCommands);
                commands.append("putfield ").append(((ClassType) instanceType).getClassName().getName()).append(
                    "/").append(memberName).append(" ").append(makeTypeSignature(memberType)).append("\n");
            }
        }
        return commands.toString();
    }

    @Override
    public String visit(BinaryExpression binaryExpression) {
        BinaryOperator operator = binaryExpression.getBinaryOperator();
        StringBuilder commands = new StringBuilder();
        if (operator == BinaryOperator.and) {
            String label = newLabel();
            String falseLabel = "andShortCircuitFalseLabel_" + label;
            String endLabel = "andShortCircuitEndLabel_" + label;
            commands.append(binaryExpression.getFirstOperand().accept(this));
            commands.append("ifeq ").append(falseLabel).append("\n");
            commands.append(binaryExpression.getSecondOperand().accept(this));
            commands.append("goto ").append(endLabel);
            commands.append(falseLabel).append(":\n");
            commands.append("iconst_0\n");
            commands.append(endLabel).append(":\n");
        }
        else if (operator == BinaryOperator.or) {
            String label = newLabel();
            String trueLabel = "orShortCircuitTrueLabel_" + label;
            String endLabel = "orShortCircuitEndLabel_" + label;
            commands.append(binaryExpression.getFirstOperand().accept(this));
            commands.append("ifne ").append(trueLabel).append("\n");
            commands.append(binaryExpression.getSecondOperand().accept(this));
            commands.append("goto ").append(endLabel).append("\n");
            commands.append(trueLabel).append(":\n");
            commands.append("iconst_1\n");
            commands.append(endLabel).append(":\n");
        }
        else if (operator == BinaryOperator.assign) {
            commands.append(assign(binaryExpression));
            commands.append(binaryExpression.getFirstOperand().accept(this));
        }
        else {
            commands.append(binaryExpression.getFirstOperand().accept(this));
            commands.append(binaryExpression.getSecondOperand().accept(this));
            if (operator == BinaryOperator.add) {
                commands.append("iadd\n");
            } else if (operator == BinaryOperator.sub) {
                commands.append("isub\n");
            } else if (operator == BinaryOperator.mult) {
                commands.append("imul\n");
            } else if (operator == BinaryOperator.div) {
                commands.append("idiv\n");
            } else if (operator == BinaryOperator.mod) {
                commands.append("irem\n");
            } else if ((operator == BinaryOperator.gt) || (operator == BinaryOperator.lt)) {
                commands.append(compare(operator.toString(), "i"));
            } else if ((operator == BinaryOperator.eq) || (operator == BinaryOperator.neq)) {
                Type operandType = binaryExpression.getFirstOperand().accept(expressionTypeChecker);
                String objectType = ((operandType instanceof IntType) || (operandType instanceof BoolType)) ? "i" : "a";
                String operatorStr = operator == BinaryOperator.neq ? "ne" : operator.toString();
                commands.append(compare(operatorStr, objectType));
            }
        }

        return commands.toString();
    }

    @Override
    public String visit(UnaryExpression unaryExpression) {
        UnaryOperator operator = unaryExpression.getOperator();
        StringBuilder commands = new StringBuilder();
        Map<UnaryOperator, BinaryOperator> unaryToBinaryOperatorMapping = Map.of(
            UnaryOperator.predec, BinaryOperator.sub,
            UnaryOperator.preinc, BinaryOperator.add,
            UnaryOperator.postdec, BinaryOperator.sub,
            UnaryOperator.postinc, BinaryOperator.add
        );
        if (operator == UnaryOperator.minus) {
            commands.append(unaryExpression.getOperand().accept(this));
            commands.append("ineg\n");
        }
        else if (operator == UnaryOperator.not) {
            String label = newLabel();
            String trueLabel = "unaryNotTrueLabel_" + label;
            String endLabel = "unaryNotEndLabel_" + label;
            commands.append(unaryExpression.getOperand().accept(this));
            commands.append("ifeq ").append(trueLabel).append("\n");
            commands.append("iconst_0\n");
            commands.append("goto ").append(endLabel).append("\n");
            commands.append(trueLabel).append(":\n");
            commands.append("iconst_1\n");
            commands.append(endLabel).append(":\n");
        }
        else if ((operator == UnaryOperator.predec) || (operator == UnaryOperator.preinc)) {
            BinaryOperator binaryOperator = unaryToBinaryOperatorMapping.get(operator);
            commands.append(
                assign(
                    new BinaryExpression(
                        unaryExpression.getOperand(),
                        new BinaryExpression(
                            unaryExpression.getOperand(),
                            new IntValue(1),
                            binaryOperator
                        ),
                        BinaryOperator.assign
                    )
                )
            );
            commands.append(unaryExpression.getOperand().accept(this));
        }
        else if ((operator == UnaryOperator.postdec) || (operator == UnaryOperator.postinc)) {
            commands.append(unaryExpression.getOperand().accept(this));
            BinaryOperator binaryOperator = unaryToBinaryOperatorMapping.get(operator);
            commands.append(
                assign(
                    new BinaryExpression(
                        unaryExpression.getOperand(),
                        new BinaryExpression(
                            unaryExpression.getOperand(),
                            new IntValue(1),
                            binaryOperator
                        ),
                        BinaryOperator.assign
                    )
                )
            );
        }
        return commands.toString();
    }

    @Override
    public String visit(ObjectOrListMemberAccess objectOrListMemberAccess) {
        Type memberType = objectOrListMemberAccess.accept(expressionTypeChecker);
        Type instanceType = objectOrListMemberAccess.getInstance().accept(expressionTypeChecker);
        String memberName = objectOrListMemberAccess.getMemberName().getName();
        String commands = "";
        if (instanceType instanceof ClassType) {
            String className = ((ClassType) instanceType).getClassName().getName();
            try {
                SymbolTable classSymbolTable = ((ClassSymbolTableItem) SymbolTable.root.getItem(ClassSymbolTableItem.START_KEY + className, true)).getClassSymbolTable();
                try {
                    classSymbolTable.getItem(FieldSymbolTableItem.START_KEY + memberName, true);
                    commands += objectOrListMemberAccess.getInstance().accept(this);
                    commands += "getfield " + ((ClassType) instanceType).getClassName().getName() + "/"
                        + memberName + " " + makeTypeSignature(memberType) + "\n";
                    commands += toPrimitive(memberType);
                } catch (ItemNotFoundException memberIsMethod) {
                    commands += "new Fptr\n";
                    commands += "dup\n";
                    commands += objectOrListMemberAccess.getInstance().accept(this);
                    commands += "ldc \"" + memberName + "\"\n";
                    commands += "invokespecial Fptr/<init>(Ljava/lang/Object;Ljava/lang/String;)V\n";
                }
            } catch (ItemNotFoundException classNotFound) {
            }
        }
        else if (instanceType instanceof ListType) {
            commands += objectOrListMemberAccess.getInstance().accept(this);
            int index = 0;
            Type elementType = null;
            for (ListNameType element : ((ListType) instanceType).getElementsTypes()) {
                if (element.getName().getName().equals(memberName)) {
                    elementType = element.getType();
                    break;
                }
                index++;
            }
            commands += "ldc " + index + "\n";
            commands += "invokevirtual List/getElement(I)Ljava/lang/Object;\n";
            commands += cast(elementType);
            commands += toPrimitive(elementType);
        }
        return commands;
    }

    @Override
    public String visit(Identifier identifier) {
        StringBuilder commands = new StringBuilder();
        int index = slotOf(identifier.getName());
        commands.append("aload ").append(index).append("\n");
        commands.append(toPrimitive(identifier.accept(expressionTypeChecker)));
        return commands.toString();
    }

    @Override
    public String visit(ListAccessByIndex listAccessByIndex) {
        StringBuilder commands = new StringBuilder();
        commands.append(listAccessByIndex.getInstance().accept(this));
        commands.append(listAccessByIndex.getIndex().accept(this));
        commands.append("invokevirtual List/getElement(I)Ljava/lang/Object;\n");
        int index = 0;
        if (listAccessByIndex.getIndex() instanceof IntValue)
            index = ((IntValue) listAccessByIndex.getIndex()).getConstant();
        Type destinationType = (
            (ListType) listAccessByIndex.getInstance().accept(expressionTypeChecker)
        ).getElementsTypes().get(index).getType();
        commands.append(cast(destinationType));
        commands.append(toPrimitive(destinationType));
        return commands.toString();
    }

    @Override
    public String visit(MethodCall methodCall) {
        StringBuilder commands = new StringBuilder();
        commands.append(" ; --------- New method Call\n");
        commands.append(emptyArrayList());
        int arraySlot = slotOf("");
        commands.append("astore ").append(arraySlot).append("\n");
        commands.append(" ; --------- Adding method call instance\n");
        Expression instance = methodCall.getInstance();
        commands.append(instance.accept(this));
        commands.append(" ; --------- Adding args\n");
        for (Expression arg : methodCall.getArgs()) {
            commands.append("aload ").append(arraySlot).append("\n");
            commands.append(arg.accept(this));
            commands.append(toObj(arg.accept(expressionTypeChecker)));
            commands.append("invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z\n");
            commands.append("pop\n");
        }
        commands.append(" ; --------- Invoking\n");
        commands.append("aload ").append(arraySlot).append("\n");
        commands.append("invokevirtual Fptr/invoke(Ljava/util/ArrayList;)Ljava/lang/Object;\n");
        Type returnType = ((FptrType) methodCall.getInstance().accept(expressionTypeChecker)).getReturnType();
        commands.append(cast(returnType));
        commands.append(toPrimitive(returnType));
        commands.append("\n");
        return commands.toString();
    }

    @Override
    public String visit(NewClassInstance newClassInstance) {
        StringBuilder commands = new StringBuilder();
        StringBuilder args = new StringBuilder();
        String className = newClassInstance.getClassType().getClassName().getName();
        commands.append("new ").append(className).append("\n");
        commands.append("dup\n");
        for (Expression arg : newClassInstance.getArgs()) {
            commands.append(arg.accept(this));
            Type argType = arg.accept(expressionTypeChecker);
            commands.append(toObj(argType));
            commands.append("\n");
            args.append(makeTypeSignature(argType));
        }
        commands.append("invokespecial ").append(className).append("/<init>(").append(args).append(")V\n");
        return commands.toString();
    }

    @Override
    public String visit(ThisClass thisClass) {
        return "aload 0\n";
    }

    @Override
    public String visit(ListValue listValue) {
        StringBuilder commands = new StringBuilder();
        commands.append(emptyArrayList());
        int arraySlot = slotOf("");
        commands.append("astore ").append(arraySlot).append("\n");
        for (Expression element : listValue.getElements()) {
            commands.append("aload ").append(arraySlot).append("\n");
            commands.append(element.accept(this));
            Type elementType = element.accept(expressionTypeChecker);
            commands.append(toObj(elementType));
            commands.append("invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z\n");
            commands.append("pop\n");
        }
        commands.append(newList(arraySlot));
        return commands.toString();
    }

    @Override
    public String visit(NullValue nullValue) {
        return "aconst_null\n";
    }

    @Override
    public String visit(IntValue intValue) {
        return "ldc " + intValue.getConstant() + "\n";
    }

    @Override
    public String visit(BoolValue boolValue) {
        int intEquivalent = boolValue.getConstant() ? 1 : 0;
        return "ldc " + intEquivalent + "\n";
    }

    @Override
    public String visit(StringValue stringValue) {
        return "ldc " + "\"" + stringValue.getConstant() + "\"\n";
    }

}