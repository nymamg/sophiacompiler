package main.visitor.utils;

import main.ast.types.NoType;
import main.ast.types.NullType;
import main.ast.types.Type;
import main.ast.types.functionPointer.FptrType;
import main.ast.types.list.ListNameType;
import main.ast.types.list.ListType;
import main.ast.types.single.BoolType;
import main.ast.types.single.ClassType;
import main.ast.types.single.IntType;
import main.ast.types.single.StringType;
import main.symbolTable.utils.graph.Graph;

import java.util.ArrayList;
import java.util.Iterator;

public class SubtypeChecker {
    private final Graph<String> classHierarchy;

    public SubtypeChecker (Graph<String> classHierarchy) {
        this.classHierarchy = classHierarchy;
    }

    public boolean isSubType (Type childType, Type parentType) {
        if (childType instanceof NoType) {
            return true;
        }

        if (childType instanceof ClassType && parentType instanceof ClassType) {
            return classHierarchy.isSecondNodeAncestorOf(
                ((ClassType) childType).getClassName().getName(),
                ((ClassType) parentType).getClassName().getName()
            );
        }

        if (
            childType instanceof NullType && (parentType instanceof FptrType || parentType instanceof ClassType)
        ) {
            return true;
        }

        if (childType instanceof FptrType && parentType instanceof FptrType) {
            ArrayList<Type> childArgs = ((FptrType) childType).getArgumentsTypes();
            ArrayList<Type> parentArgs = ((FptrType) parentType).getArgumentsTypes();
            if (childArgs.size() != parentArgs.size()) {
                return false;
            }
            else {
                Iterator<Type> childIter = childArgs.iterator();
                Iterator<Type> parentIter = parentArgs.iterator();
                while (childIter.hasNext() && parentIter.hasNext()) {
                    if (!isSubType(parentIter.next(), childIter.next())) {
                        return false;
                    }
                }
            }
            return isSubType(((FptrType) childType).getReturnType(), ((FptrType) parentType).getReturnType());
        }

        if (childType instanceof ListType && parentType instanceof ListType) {
            ArrayList<ListNameType> childElementTypes = ((ListType) childType).getElementsTypes();
            ArrayList<ListNameType> parentElementTypes = ((ListType) parentType).getElementsTypes();
            if (childElementTypes.size() != parentElementTypes.size()) {
                return false;
            }
            else {
                Iterator<ListNameType> childIter = childElementTypes.iterator();
                Iterator<ListNameType> parentIter = parentElementTypes.iterator();
                while (childIter.hasNext() && parentIter.hasNext()) {
                    if (!isSubType(childIter.next().getType(), parentIter.next().getType())) {
                        return false;
                    }
                }
                return true;
            }
        }

        if (
            (childType instanceof IntType && parentType instanceof IntType)
            || (childType instanceof BoolType && parentType instanceof BoolType)
            || (childType instanceof StringType && parentType instanceof StringType)
            || (childType instanceof NullType && parentType instanceof NullType)
        ) {
            return childType.toString().equals(parentType.toString());
        }
        return false;
    }
}
