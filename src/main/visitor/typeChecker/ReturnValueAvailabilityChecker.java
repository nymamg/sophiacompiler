package main.visitor.typeChecker;

import main.ast.nodes.declaration.classDec.classMembersDec.MethodDeclaration;
import main.ast.nodes.statement.*;
import main.ast.nodes.statement.loop.BreakStmt;
import main.ast.nodes.statement.loop.ContinueStmt;
import main.ast.nodes.statement.loop.ForStmt;
import main.ast.nodes.statement.loop.ForeachStmt;
import main.ast.types.NullType;
import main.compileErrorException.typeErrors.MissingReturnStatement;
import main.visitor.Visitor;

public class ReturnValueAvailabilityChecker extends Visitor<Boolean> {

    @Override
    public Boolean visit(MethodDeclaration methodDeclaration) {
        if (methodDeclaration.getReturnType() instanceof NullType) {
            return null;
        }
        for (Statement statement: methodDeclaration.getBody()) {
            if (statement.accept(this)) {
                return true;
            }
        }
        methodDeclaration.addError(new MissingReturnStatement(methodDeclaration));
        return true;
    }

    @Override
    public Boolean visit(ConditionalStmt conditionalStmt) {
        boolean thenBodyReturns = conditionalStmt.getThenBody().accept(this);
        if (conditionalStmt.getElseBody() != null) {
            return thenBodyReturns && conditionalStmt.getElseBody().accept(this);
        }
        return false;
    }

    @Override
    public Boolean visit (ForeachStmt foreachStmt) {
        return false;
    }

    @Override
    public Boolean visit (ForStmt forStmt) {
        return false;
    }

    @Override
    public Boolean visit(BlockStmt blockStmt) {
        for (Statement statement: blockStmt.getStatements()) {
            if (statement.accept(this)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean visit(AssignmentStmt assignmentStmt) {
        return false;
    }

    @Override
    public Boolean visit(PrintStmt printStmt) {
        return false;
    }

    @Override
    public Boolean visit(ReturnStmt returnStmt) {
        return true;
    }

    @Override
    public Boolean visit(MethodCallStmt methodCallStmt) {
        return false;
    }

    @Override
    public Boolean visit(ContinueStmt continueStmt) {
        return false;
    }

    @Override
    public Boolean visit(BreakStmt breakStmt) {
        return false;
    }
}
