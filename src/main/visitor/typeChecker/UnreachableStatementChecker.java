package main.visitor.typeChecker;

import main.ast.nodes.declaration.classDec.classMembersDec.ConstructorDeclaration;
import main.ast.nodes.declaration.classDec.classMembersDec.MethodDeclaration;
import main.ast.nodes.statement.*;
import main.ast.nodes.statement.loop.BreakStmt;
import main.ast.nodes.statement.loop.ContinueStmt;
import main.ast.nodes.statement.loop.ForStmt;
import main.ast.nodes.statement.loop.ForeachStmt;
import main.compileErrorException.typeErrors.UnreachableStatements;
import main.visitor.Visitor;

import java.util.ArrayList;



public class UnreachableStatementChecker extends Visitor<Integer> {
    static final int CAN_RESUME = 0;
    static final int CONTINUE_OR_BREAK = 1;
    static final int RETURN = 2;

    private int nestedLoopCount = 0;

    @Override
    public Integer visit(ConstructorDeclaration constructorDeclaration) {
        return visit((MethodDeclaration) constructorDeclaration);
    }

    @Override
    public Integer visit(MethodDeclaration methodDeclaration) {
        ArrayList<Statement> statements = methodDeclaration.getBody();
        for (int i = 0; i < statements.size(); i++) {
            if (statements.get(i).accept(this) == RETURN) {
                if ((i < statements.size() - 1)) {
                    statements.get(i+1).addError(new UnreachableStatements(statements.get(i+1)));
                }
            }
        }
        return CAN_RESUME;
    }

    @Override
    public Integer visit(ConditionalStmt conditionalStmt) {
        int thenBodyResult = conditionalStmt.getThenBody().accept(this);
        if (conditionalStmt.getElseBody() != null && thenBodyResult != CAN_RESUME) {
            int elseBodyResult = conditionalStmt.getElseBody().accept(this);
            if (elseBodyResult != CAN_RESUME) {
                if (thenBodyResult == RETURN && elseBodyResult == RETURN) {
                    return RETURN;
                }
                return CONTINUE_OR_BREAK;
            }
        }
        return CAN_RESUME;
    }

    @Override
    public Integer visit (ForeachStmt foreachStmt) {
        nestedLoopCount++;
        foreachStmt.getBody().accept(this);
        nestedLoopCount--;
        return CAN_RESUME;
    }

    @Override
    public Integer visit (ForStmt forStmt) {
        nestedLoopCount++;
        forStmt.getBody().accept(this);
        nestedLoopCount--;
        return CAN_RESUME;
    }

    @Override
    public Integer visit(BlockStmt blockStmt) {
        ArrayList<Statement> statements = blockStmt.getStatements();
        int finalResult = CAN_RESUME;
        for (int i = 0; i < statements.size(); i++) {
            int result = statements.get(i).accept(this);
            if (result != CAN_RESUME) {
                if (finalResult == CAN_RESUME) {
                    finalResult = result;
                }
                if ((i < statements.size() - 1)) {
                    statements.get(i+1).addError(new UnreachableStatements(statements.get(i+1)));
                }
            }
        }
        return finalResult;
    }

    @Override
    public Integer visit(AssignmentStmt assignmentStmt) {
        return CAN_RESUME;
    }

    @Override
    public Integer visit(PrintStmt printStmt) {
        return CAN_RESUME;
    }

    @Override
    public Integer visit(ReturnStmt returnStmt) {
        return RETURN;
    }

    @Override
    public Integer visit(MethodCallStmt methodCallStmt) {
        return CAN_RESUME;
    }

    @Override
    public Integer visit(ContinueStmt continueStmt) {
        if (nestedLoopCount > 0) {
            return CONTINUE_OR_BREAK;
        }
        return CAN_RESUME;
    }

    @Override
    public Integer visit(BreakStmt breakStmt) {
        if (nestedLoopCount > 0) {
            return CONTINUE_OR_BREAK;
        }
        return CAN_RESUME;
    }
}
