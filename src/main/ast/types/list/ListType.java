package main.ast.types.list;

import main.ast.types.NoType;
import main.ast.types.Type;
import main.compileErrorException.typeErrors.ElementsOfDifferingTypes;
import main.compileErrorException.typeErrors.NoSuchMember;
import main.symbolTable.utils.graph.Graph;
import main.visitor.utils.SubtypeChecker;

import java.util.ArrayList;

public class ListType extends Type {
    private ArrayList<ListNameType> elementsTypes = new ArrayList<>();

    public ListType() {
    }

    public ListType(ArrayList<ListNameType> elementsTypes) {
        this.elementsTypes = elementsTypes;
    }

    public ListType(int listSize, ListNameType listNameType) {
        for(int i=0; i < listSize; i++) {
            elementsTypes.add(listNameType);
        }
    }

    public ArrayList<ListNameType> getElementsTypes() {
        return elementsTypes;
    }

    public void setElementsTypes(ArrayList<ListNameType> elementsTypes) {
        this.elementsTypes = elementsTypes;
    }

    public void addElementType(ListNameType listNameType) {
        this.elementsTypes.add(listNameType);
    }

    public Type getElementsType(Graph<String> classHierarchy) throws ElementsOfDifferingTypes {
        SubtypeChecker subtypeChecker = new SubtypeChecker(classHierarchy);
        Type listElementType = null;
        for (ListNameType element : elementsTypes) {
            if (listElementType == null) {
                listElementType = element.getType();
            }
            else if (element.getType() instanceof NoType) {
                continue;
            }
            else if (!(
                subtypeChecker.isSubType(listElementType, element.getType())
                    && subtypeChecker.isSubType(element.getType(), listElementType)
                )) {
                throw new ElementsOfDifferingTypes();
            }
        }
        return listElementType;
    }

    public ListNameType getMember(String memberName) throws NoSuchMember {
        for (ListNameType member : elementsTypes) {
            if (member.getName().getName().equals(memberName)) {
                return member;
            }
        }
        throw new NoSuchMember();
    }

    @Override
    public String toString() {
        return "ListType";
    }
}